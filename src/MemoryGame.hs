module MemoryGame where

import           Control.Monad (join)
import           Data.Sequence (Seq, fromList, update, (!?))
import           GameState

type Index = Int
type CardValue = Int

type Card = (Index, CardValue)

type MemoryBoard = Seq (Maybe CardValue)

getCard :: MemoryBoard -> Index -> Either Error Card
getCard board index
  | index < 0 = Left "Negative index."
  | index >= length board = Left "Index greater than board length"
  | otherwise = let maybeCard = join $ board !? index
                in maybe (Left "That card is already gone.") (\c -> Right (index, c)) maybeCard

data MemoryTurn = NoCards | OneCard Card | TwoCards Card Card

isCardSelected :: MemoryTurn -> Card -> Bool
isCardSelected turn card =
  case turn of
    NoCards               -> False
    OneCard selectedCard  -> card == selectedCard
    TwoCards first second -> first == card || second == card

type Error = String

data MemoryGameRep = MemoryGameRep {
  gameBoard :: MemoryBoard,
  gameTurn  :: MemoryTurn,
  gameScore :: Int,
  errors    :: [Error]
}

addError :: Error -> MemoryGameRep -> MemoryGameRep
addError error rep =
  let oldErrors = errors rep
  in rep {errors = oldErrors ++ [error]}

type MemoryGameState = GameState WinOnlyEndStateWithScore MemoryGameRep

createGame :: [Int] -> MemoryGameState
createGame cardValues =
  let newBoard = fromList $ map Just cardValues
      initialGameTurn = NoCards
      initialScore = 0
      initialErrors = []
  in Ongoing $ MemoryGameRep newBoard initialGameTurn initialScore initialErrors

runMove :: Index -> MemoryGameRep -> MemoryGameRep
runMove index memoryGameRep =
  let board = gameBoard memoryGameRep
      errorOrCard = getCard board index
  in case errorOrCard of
    Left error -> addError error memoryGameRep
    Right card ->
      case gameTurn memoryGameRep of
        TwoCards _ _ -> addError "Two cards have already been selected.  No action permitted." memoryGameRep
        OneCard otherCard -> if card == otherCard then addError "Can't pick the same card twice!" memoryGameRep else memoryGameRep { gameTurn = TwoCards otherCard card }
        NoCards -> memoryGameRep { gameTurn = OneCard card }

clearErrors :: MemoryGameRep -> MemoryGameRep
clearErrors rep = rep {errors = []}

roundOver :: MemoryGameRep -> Bool
roundOver rep =
  case gameTurn rep of
    TwoCards _ _ -> True
    _ -> False

processTurn :: MemoryGameRep -> MemoryGameState
processTurn rep =
  if gameOver rep
  then Finished $ Won (gameScore rep)
  else case gameTurn rep of
    TwoCards (index_1, value_1) (index_2, value_2) ->
      if value_1 == value_2
      then let newBoard = update index_1 Nothing $ update index_2 Nothing $ gameBoard rep
               newScore = gameScore rep + value_1
               newGameRep = rep { gameBoard = newBoard, gameScore = newScore, gameTurn = NoCards }
           in if gameOver newGameRep then Finished (Won $ gameScore newGameRep) else  Ongoing newGameRep
      else Ongoing $ rep { gameTurn = NoCards }
    _ -> Ongoing rep

gameOver :: MemoryGameRep -> Bool
gameOver = let allNothing = all (==Nothing)
           in allNothing . gameBoard
