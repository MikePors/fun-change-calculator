module ChangeCalculation ( compute,
                           negativePayment,
                           negativePrice,
                           priceNotPaid,
                           youAreBoring,
                           Price,
                           Payment ) where


type Price = Int
type Payment = Int

negativePayment :: String
negativePayment = "A negative payment?  Really?"

negativePrice :: String
negativePrice = "Something has gone wrong.  We are trying to give you a negative price.  Like jerks."

priceNotPaid :: String
priceNotPaid = "That's not enough money. Go away!"

youAreBoring :: String
youAreBoring = "You paid in exact change.  How dreadfully boring."

compute :: Price -> Payment -> Either String [Int]
compute price payment
  | price < 0 = Left negativePrice
  | payment < 0 = Left negativePayment
  | payment < price = Left priceNotPaid
  | payment == price = Left youAreBoring
  | otherwise = let change = payment - price
                in Right $ partitionChange change

partitionChange :: Int -> [Int]
partitionChange change =
  let initialBreakdown = breakdown change
      padding = max 0 (20 - 2 * length initialBreakdown)
  in initialBreakdown ++ initialBreakdown ++ replicate padding 0

breakdown :: Int -> [Int]
breakdown x =
  let upper = floor $ (sqrt (1.0 + 8.0 * fromIntegral x) - 1.0) / 2.0
      xs = [1..upper]
      remainder = x - sum xs
  in remainder : xs
