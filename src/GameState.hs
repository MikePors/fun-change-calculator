module GameState where

import Control.Monad
import Control.Applicative

data GameState e a = Finished e | Ongoing a

instance Functor (GameState e) where
  fmap f x = case x of
    Finished y -> Finished y
    Ongoing x' -> Ongoing $ f x'

newtype WinOnlyEndStateWithScore = Won Int deriving (Show)
