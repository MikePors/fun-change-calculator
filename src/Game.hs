module Game
    ( play,
      run
    ) where

import           GameState
import           MemoryGame
import           MemoryGameView

import qualified ChangeCalculation

import           Control.Monad         (when)
import           System.Random         (newStdGen, getStdRandom, randomR)
import           System.Random.Shuffle (shuffle')

play :: IO ()
play = do
  dollars <- getStdRandom $ randomR (1, 99)
  cents <- getStdRandom $ randomR (1, 99)
  let centsString = if cents < 10 then '0' : show cents else show cents
  putStrLn $ "The price you have to pay is: " ++ show dollars ++ '.' : show cents
  payment <- getPaymentInDollars
  let breakdown = ChangeCalculation.compute (dollars * 100 + cents) (payment * 100)
  case breakdown of
    Left message -> display message
    Right cardValues -> do
      stdGen <- newStdGen
      let shuffledValues = shuffle' cardValues (length cardValues) stdGen
          game = createGame shuffledValues
      run game

run :: MemoryGameState -> IO ()
run gameState =
  case gameState of
    Finished (Won score) -> do
      clearScreen
      display $ "You got $" ++ show (fromIntegral score / 100.0) ++ " in change!"
    Ongoing gameRep -> do
      renderMemoryGame gameRep
      index <- getPlayerMove
      let nextGameStep = runMove index gameRep
      when (roundOver nextGameStep) $ renderMemoryGame nextGameStep >> putStrLn "\n" >> pressEnter
      run $ processTurn nextGameStep

pressEnter :: IO ()
pressEnter = do
  putStrLn "Press Enter"
  getLine
  return ()
