module MemoryGameView ( clearScreen,
                        renderMemoryGame,
                        display,
                        getPlayerMove,
                        getPaymentInDollars ) where

import           Control.Arrow                ((&&&))
import           Data.Sequence                (Seq, intersperse, mapWithIndex,
                                               replicate, zipWith, (<|), (><),
                                               (|>), drop, take)
import           Control.Monad                 (unless)

import           MemoryGame
import           Prelude                      hiding (zipWith, drop, take)
import           System.Console.ANSI          (clearScreen)
import           System.Console.Terminal.Size (Window, height, size, width)
import           Text.Read                    (readMaybe)

data CardView = None | Hidden | Showing Int

data Indexed a = Indexed Int a

fromSequence :: Seq a -> Seq (Indexed a)
fromSequence = mapWithIndex Indexed

getPaymentInDollars :: IO Int
getPaymentInDollars = getIntWith "How much are you paying?" "... that's not a number..."

getPlayerMove :: IO Int
getPlayerMove = getIntWith "Pick a card to flip over" "srly?"

getIntWith :: String -> String -> IO Int
getIntWith prompt errorMessage = do
  putStrLn prompt
  numStr <- getLine
  let maybeInt = readMaybe numStr :: Maybe Int
  maybe (putStrLn errorMessage >> getIntWith prompt errorMessage) return maybeInt

replicateS :: Int -> a -> Seq a
replicateS = Data.Sequence.replicate

asView :: MemoryBoard -> MemoryTurn -> Seq CardView
asView board turn =
  mapWithIndex (\index  maybeCard -> maybe None (\card -> viewFromCard turn (index, card)) maybeCard) board


viewFromCard :: MemoryTurn -> Card -> CardView
viewFromCard turn card@(_, value) =
  if isCardSelected turn card
  then Showing value
  else Hidden

renderMemoryGame :: MemoryGameRep -> IO()
renderMemoryGame rep =
  do
    clearScreen
    maybeWindow <- getWindowDimensions
    maybe (print "Something went wrong") (renderMemoryGameWithWindow rep) maybeWindow


renderMemoryGameWithWindow :: MemoryGameRep -> WindowDimensions -> IO ()
renderMemoryGameWithWindow (MemoryGameRep board turn score errors) window =
  do
    printScore  window score
    let views = asView board turn
    printViews window (fromSequence views)
    printErrors window errors

type WindowDimensions = Window Int

getWindowDimensions :: IO (Maybe WindowDimensions)
getWindowDimensions = size

printScore :: WindowDimensions -> Int -> IO()
printScore dimensions score =
  let windowWidth = width dimensions
      scoreString = "You've accumulated $" ++ show (fromIntegral score / 100.0) ++ " of your change"
      shortScoreString = "Score: " ++ show score
      scoreStringLength = length scoreString
      shortScoreStringLength = length shortScoreString
  in if windowWidth >= scoreStringLength
     then putStrLn scoreString
     else if windowWidth >= shortScoreStringLength
     then putStrLn shortScoreString
     else putStrLn scoreString

printViews :: WindowDimensions -> Seq (Indexed CardView) -> IO()
printViews dimensions indexedViews =
  let cardWidth = 5
      cardHeight = 7
      rowLength = 10
  in doPrintViews rowLength cardWidth cardHeight indexedViews

doPrintViews :: Int -> Int -> Int -> Seq (Indexed CardView) -> IO()
doPrintViews rowLength cardWidth cardHeight indexedViews =
  let currentRow = take rowLength indexedViews
      cardStrings = fmap (viewToStrings cardWidth cardHeight) currentRow
      formattedCardStrings = intersperse (replicateS (cardHeight + 1) " ") cardStrings
      outputStrings = foldr (zipWith (++)) (replicateS (cardHeight + 1) "") formattedCardStrings
  in unless (null indexedViews) (mapM_ putStrLn outputStrings >> doPrintViews rowLength cardWidth cardHeight (drop rowLength indexedViews))

type CardWidth = Int
type CardHeight = Int

viewToStrings :: CardWidth -> CardHeight -> Indexed CardView -> Seq String
viewToStrings width height (Indexed index view) =
  let indexStr = show index
      indexLine = ' ' : '#' : indexStr ++ Prelude.replicate (width - length indexStr - 2) ' '
  in case view of
    None -> let emptyCardString = Prelude.replicate width ' '
            in replicateS (height + 1) emptyCardString
    Hidden -> let topAndBottom = Prelude.replicate width '-'
                  middle = '|': Prelude.replicate (width - 2) '*' ++ ['|']
              in topAndBottom <| ((replicateS (height - 2) middle |> topAndBottom) |> indexLine)
    Showing val -> let topAndBottom = Prelude.replicate width '-'
                       background = '|' : Prelude.replicate (width - 2) ' ' ++ ['|']
                       valStr = show val
                       valLength = length valStr
                       leadWidth = (width - valLength - 2) `quot` 2
                       leadHeight = (height - 1 - 2) `quot` 2
                       foreground = '|' : Prelude.replicate leadWidth ' ' ++ valStr ++ Prelude.replicate (width - leadWidth - 2 - valLength) ' '  ++ "|"
                   in ((topAndBottom <| replicateS leadHeight background) >< ((foreground <| replicateS (height - leadHeight - 2 - valLength) background) |> topAndBottom)) |> indexLine

printErrors :: WindowDimensions -> [Error] -> IO ()
printErrors _ = mapM_ putStrLn

display :: String -> IO()
display = putStrLn
