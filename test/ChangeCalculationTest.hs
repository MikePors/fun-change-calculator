module ChangeCalculationTest where

import Control.Applicative

import Test.Hspec
import Test.QuickCheck

import ChangeCalculation

test :: IO ()
test = hspec $
  describe "compute" $ do
    it "is rude to the user when the user gives negative change" $
      compute 20 (-1) `shouldBe` Left negativePayment
    it "admits that the system is being rude when the user is given a negative price" $
      compute (-20) 1 `shouldBe` Left negativePrice
    it "is rude to the user when they try to pay less than the price" $
      compute 20 10 `shouldBe` Left priceNotPaid
    it "tells the user they are boring if they pay in exact change" $
      compute 20 20 `shouldBe` Left youAreBoring
    it "returns a list of positive integers" $
      property $ forAll positiveIncreasingIntegerPairs (\(x, y) -> (Right (all (>=0)) <*> compute x y) == Right True)
    it "returns an a list which sums to the change" $
      property $ forAll positiveIncreasingIntegerPairs (\(price, paid) -> (Right sum <*> compute price paid) == Right (paid - price))
generatePositiveInt :: Gen (Positive Int)
generatePositiveInt = arbitrary

positiveIncreasingIntegerPairs :: Gen (Int, Int)
positiveIncreasingIntegerPairs = do
  n <- fmap getPositive generatePositiveInt
  m <- fmap getPositive generatePositiveInt
  return (n, n + m)
